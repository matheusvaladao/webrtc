navigator.webkitGetUserMedia({video:true, audio:true}, function(stream){

  var Peer = require('simple-peer')
  var peer = new Peer({
    initiator: location.hash === '#init',
    trickle: false,
    stream: stream
  })

  peer.on('signal', function (data) {
    document.getElementById('yourId').value = JSON.stringify(data)
  })

  document.getElementById('connect').addEventListener('click', function () {
    var otherId = JSON.parse(document.getElementById('otherId').value)
    peer.signal(otherId)
  })

  document.getElementById('send').addEventListener('click', function () {
    var yourMessage = document.getElementById('yourMessage').value
    document.getElementById('messages').textContent += 'Você disse:'+ document.getElementById('yourMessage').value +'\n\n'
    peer.send(yourMessage)
  })

  peer.on('data', function (data) {
    document.getElementById('messages').textContent += 'Parceiro disse:'+data + '\n\n'
  })

  peer.on('stream', function (stream) {

    var video = document.getElementById('video')
    video.src = window.URL.createObjectURL(stream)
    video.play()
    
  })

}, function(err){
  console.error(err)
})
